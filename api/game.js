import {GET} from "./base.js";

export const getGameList = params => GET(`/game/${params.week}`, params)

export const getCurrentGameList = params => GET(`/game/current`, params)