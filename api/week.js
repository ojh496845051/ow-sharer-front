import {GET} from "./base.js";

export const getWeekList = () => GET('/week')