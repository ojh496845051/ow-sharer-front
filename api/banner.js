import {GET} from "./base.js";

export const getBannerList = params => GET(`/banner`, params)