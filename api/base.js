import {
	tryShowLoading,
	tryHideLoading
} from "../utils/loading.js"

// const BASE_URL = 'http://39.100.250.102/mock/5ea28a2786046725ea214447/example'
const BASE_URL = 'http://127.0.0.1:8080'

const request = (method, url, data, config) => {
	tryShowLoading()
	return new Promise((resolve, reject) => {
		uni.request({
			url: url.startsWith('http') ? url : BASE_URL + url,
			data,
			method,
			timeout: 30000,
			header: {
				Accept: "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9"
			},
			success: res => {
				resolve(res.data)
			},
			fail: error => {
				reject(error)
			},
			complete: tryHideLoading
		})
	})
}

export const GET = (url, data, config) => request('GET', url, data, config)

export const POST = (url, data, config) => request('POST', url, data, config)

export const PUT = (url, data, config) => request('PUT', url, data, config)

export const DELETE = (url, data, config) => request('DELETE', url, data, config)

export default {
	GET,
	POST,
	PUT,
	DELETE
}
