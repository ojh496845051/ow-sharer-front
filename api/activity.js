import {GET} from "./base.js";

export const getActivityList = params => GET(`/activity`, params)