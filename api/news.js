import {GET} from "./base.js";

export const getNewsList = params => GET('/news', params);